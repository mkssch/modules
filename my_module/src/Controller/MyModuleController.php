<?php

namespace Drupal\my_module\Controller;

class MyModuleController{
    public function test(){
        $output = array();

        $output['#title'] = 'HelloWorld page title';

        $output['#markup'] = 'Hello World!';

        return $output;
    }
}