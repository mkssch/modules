<?php
/**
 * Created by PhpStorm.
 * User: Kostya
 * Date: 012 12.04.18
 * Time: 15:34
 */
namespace Drupal\ex_form\Form;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class ExForm extends FormBase
{

    // метод, который отвечает за саму форму - кнопки, поля
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['firstname'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Ваше имя'),
            '#required' => TRUE,
        ];

        $form['lastname'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Ваше фамилия'),
        ];

//        $form['subject'] = [
//            '#type' => 'textfield',
//            '#title' => $this->t('Тема сообщения'),
//        ];
//        $form['message'] = [
//            '#type' => 'textfield',
//            '#title' => $this->t('Текст сообщения'),
//            '#required' => TRUE,
//        ];
        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Email'),
            '#required' => TRUE,
        ];

        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Отправить сообщение'),
        ];

        return $form;
    }

    // метод, который будет возвращать название формы
    public function getFormId()
    {
        return 'ex_form_exform_form';
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $email = $form_state->getValue('email');
        $pattern = "/^[a-z0-9_][a-z0-9\._-]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+$/i";
        if (!preg_match($pattern, $email)) {
            $form_state->setErrorByName('email', $this->t('Неверно введеен Email'));
        }

    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        drupal_set_message(t('Сообщение успешно отправлено.'));
        $email = $form_state->getValue('email');
//        $message = $form_state->getValue('message');
        \Drupal::logger('content_entity_example')->notice('Сообщение отправлено на адрес %email.', ['%email' => $email]);
        $data = array(
           'firstname' => $form_state->getValue('firstname'),
           'lastname' => $form_state->getValue('lastname'),
           'email' => $form_state->getValue('email'),
        );
        $this->POST_webform($data);
    }

    function POST_webform(&$form_state){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.hubspot.com/companies/v2/create-contact?portalId=4476239&clienttimeout=7000");
        curl_setopt($ch, CURLOPT_POST, 1);
        $payload = json_encode($form_state);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_exec($ch);
        curl_close($ch);
    }

}